package az.ingress.lesson1;

import az.ingress.lesson1.config.ConfigProperties;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@Slf4j
@AllArgsConstructor
public class Lesson1Application implements CommandLineRunner {

	private final ConfigProperties properties;

	@Value("${ms25.lesson}")


	public static void main(String[] args) {
		SpringApplication.run(Lesson1Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info("Lesson number: {}", properties.getLesson());
	}
}
