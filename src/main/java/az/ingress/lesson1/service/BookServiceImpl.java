package az.ingress.lesson1.service;

import az.ingress.lesson1.dto.BookRequestDto;
import az.ingress.lesson1.dto.BookResponseDto;
import az.ingress.lesson1.model.Book;
import az.ingress.lesson1.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public int create(BookRequestDto dto) {
        Book book = Book.builder()
                .author(dto.getAuthor())
                .pageCount(dto.getPageCount())
                .name(dto.getName())
                .build();
        bookRepository.save(book);
        return book.getId();
    }

    @Override
    public BookResponseDto update(Integer id, BookRequestDto dto) {

        Optional<Book> optionalBook = bookRepository.findById(id);

        if (!optionalBook.isPresent()) {
            throw new RuntimeException("Book not found ");
        }

        Book existingBook = optionalBook.get();
        existingBook.setAuthor(dto.getAuthor());
        existingBook.setPageCount(dto.getPageCount());
        existingBook.setName(dto.getName());

        Book updatedBook = bookRepository.save(existingBook);

        return BookResponseDto.builder()
                .author(updatedBook.getAuthor())
                .pageCount(updatedBook.getPageCount())
                .name(updatedBook.getName())
                .id(updatedBook.getId())
                .build();
    }

    @Override
    public void delete(Integer id) {
        if (!bookRepository.existsById(id)) {
            throw new RuntimeException("Book not found ");
        }
        bookRepository.deleteById(id);
    }

    @Override
    public BookResponseDto get(Integer id) {
        Book book = bookRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Book not found"));
        return BookResponseDto.builder()
                .author(book.getAuthor())
                .pageCount(book.getPageCount())
                .name(book.getName())
                .id(book.getId())
                .build();
    }

    @Override
    public List<BookResponseDto> getAll() {
        List<Book> books = bookRepository.findAll();
        List<BookResponseDto> bookResponseDtoList = new ArrayList<>();
        for (Book book : books) {
            BookResponseDto dto = BookResponseDto.builder()
                    .id(book.getId())
                    .author(book.getAuthor())
                    .pageCount(book.getPageCount())
                    .name(book.getName())
                    .build();
            bookResponseDtoList.add(dto);
        }
        return bookResponseDtoList;
    }

}
