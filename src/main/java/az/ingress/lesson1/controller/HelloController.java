package az.ingress.lesson1.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
public class HelloController {

    @GetMapping()
    public String hello() {
        return "Hello from 2";
    }
}
