package az.ingress.lesson1.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "ms25")
@Data
public class ConfigProperties {

    private Integer lesson;
}
